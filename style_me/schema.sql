DROP TABLE IF EXISTS user;
DROP TABLE IF EXISTS wishlist;
DROP TABLE IF EXISTS comments;

CREATE TABLE user (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    username TEXT UNIQUE NOT NULL,
    password TEXT NOT NULL,
    gender BOOLEAN DEFAULT TRUE,
    fashionista BOOLEAN DEFAULT FALSE
);

CREATE TABLE IF NOT EXISTS fashion_gallery (
    image_id INTEGER PRIMARY KEY AUTOINCREMENT,
    image_file_url TEXT NOT NULL,
    image_file_name TEXT NOT NULL,
    ecommerce_link TEXT,
    price TEST,
    description TEXT,
    fashion_type TEXT
);

CREATE TABLE wishlist (
    image_file_name TEXT,
    user_id INTEGER,
    PRIMARY KEY (image_file_name, user_id)
);

CREATE TABLE comments (
    image_file_name TEXT,
    username TEXT,
    comment_text TEXT,
    created TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);
