from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for
)

import numpy as np
import keras
from keras.preprocessing import image
from keras.models import Model
from keras.applications.vgg16 import VGG16
from keras.applications.vgg16 import preprocess_input
from keras.layers import Dense
from keras.applications.resnet50 import ResNet50
from keras.preprocessing import image
from keras.applications.resnet50 import preprocess_input, decode_predictions
from keras.layers import GlobalMaxPooling2D
from PIL import Image as pil_image
from scipy.spatial import distance
from sklearn.neighbors import NearestNeighbors
from os import listdir
from os.path import isfile, join
import pickle
import cv2
import matplotlib.pyplot as plt

IMAGE_DATASET_PATH = "./images/"

class FashionRecommender():
    
    """
        Initializes the vgg16 model for fashion recommendation
    """
    def __init__(self):
        self.image_vectors_file = 'style_me/model/image-vectors.pickle'
        self.image_file_names = 'style_me/model/file-names.pickle'
        self.vgg16_model_path = 'style_me/model/vgg16.h5'
        self.resnet50_model_path = 'style_me/model/resnet50.h5'
        self.image_vectors = []
        self.image_files = []
        self.recommender = None
        self.num_recommended_images = 11
        self.model = None
     """
        pretrained model of the vgg16 for images 
    """
    def pretrained_model_vgg16(self):
        model_vgg16_conv = VGG16(weights='imagenet', include_top=True)
        # create new model that uses the input and the last fully connected layer from vgg16
        self.model = Model(inputs=model_vgg16_conv.input, outputs=model_vgg16_conv.get_layer('fc2').output)

    """
        pretrained model of the resnet50 for images 
    """
    def pretrained_model_resnet(self):
        # Pre-Trained Model
        base_model = ResNet50(weights='imagenet', include_top=False, 
                              input_shape = (224, 224, 3))
        base_model.trainable = False
        # Add Layer Embedding
        self.model = keras.Sequential([base_model,GlobalMaxPooling2D()])
    
    """
        gets the image vector as input 
    """
    def get_image_vector(self, file_path):
        # Reshape
        img = image.load_img(file_path, target_size=(244, 244))
        # img to Array
        x   = image.img_to_array(img)
        # Expand Dim (1, w, h)
        x   = np.expand_dims(x, axis=0)
        # Pre process Input
        x   = preprocess_input(x)
        return self.model.predict(x).reshape(-1)

    """
        extracts the all the images vectors using the pretrained data model 
    """
    def extract_image_features(self):
        self.image_files = sorted([f for f in listdir(IMAGE_DATASET_PATH) if isfile(join(IMAGE_DATASET_PATH, f))])
        for image_file in self.image_files:
            self.image_vectors.append(self.get_image_vector(join(IMAGE_DATASET_PATH, image_file)))
            print('extracted features ===> ', image_file)
    
     """
        get recommendations for the given input file for the image 
    """
    def get_recommendations(self, query_file_path):
        feature_vector = self.get_image_vector(query_file_path)
        distances, indices = self.recommender.kneighbors([feature_vector])
        recommended_images_file_name = [query_file_path]
        for index in indices[0]:
            recommended_images_file_name.append(self.image_files[index])
        return recommended_images_file_name
   
   """
        Initializes the recommender (current recommendation algorithm is KNN)
    """
    def init_recommender(self):
        self.recommender = NearestNeighbors(n_neighbors=self.num_recommended_images, algorithm='brute',
                                            metric='cosine').fit(self.image_vectors)


    """
        loads the pretrained model as input
    """
    def load_model(self):
        # load the pretrained model -> resnet50 or vgg16
        self.model = keras.models.load_model(self.resnet50_model_path)
        self.image_vectors = pickle.load(open(self.image_vectors_file, 'rb'))
        self.image_files = pickle.load(open(self.image_file_names, 'rb'))
        self.init_recommender()
    
    """
        Saves the current version of the model in pickle files
    """
    def save_model(self):
        self.model.save('./model/resnet50.h5')
        pickle.dump(self.image_vectors, open('./model/image-vectors.pickle', 'wb'))
        pickle.dump(self.image_files, open('./model/file-names.pickle', 'wb'))


    """
        plots the recommended images 
    """
    def plot_images(self, images_path, rows=1, cols=1):
        fig = plt.figure(figsize=(10, 7))
        images = [cv2.imread(image_path) for image_path in images_path]
        for i in range(rows * cols):
            fig.add_subplot(rows, cols, i + 1)
            plt.imshow(images[i])
            plt.axis('off')
        plt.show()

"""
    initializes the recommenders by storing the weights in the file
"""
def init_recommender():
    fr = FashionRecommender()
    fr.pretrained_model_resnet()
    fr.extract_image_features()
    fr.save_model()

"""
    loads the recommender by the readings the weights from the file
"""
def load_recommnder():
    fr = FashionRecommender()
    fr.load_model()
    return fr
