from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for
)
from werkzeug.exceptions import abort

from style_me.auth import login_required
from style_me.db import get_db
from . import recommender 

bp = Blueprint('style_me', __name__)

# load the fashion recommender form the initialized precomupted weights 
# of images which are stored in the fashion gallery 
fr = recommender.load_recommnder()

QUERY_IMAGE_PATH = "./style_me/static/"

# ======================  RESTFUL ROUTES ===================================

# provide API to major services like recommendation engine and fashion gallery
# provide links to services provided by the application
@bp.route('/')
def index():
    #return 'provide API to basic '
    return render_template('style_me/home_page.html')

# retrived images based on the file name 
def retrieve_images(file_names):
    db = get_db()
    fashion_images = []
    for file_name in file_names:
        fashion_image = db.execute("""
                                        select * from fashion_gallery where image_file_name = (?)
                                   """, (file_name, )).fetchone()
        fashion_images.append(fashion_image)
    # return the fashion images data
    return fashion_images


# gets the type of the fashion based of image file name
def retrieve_fashion_type(file_name):
    print(file_name)
    db = get_db()
    fashion_type = db.execute("""
                                select fashion_type from fashion_gallery where image_file_name = (?)
                              """, (file_name, )).fetchone()
    print(fashion_type)
    return dict(fashion_type)['fashion_type']


# route for quering the images on the applications
@bp.route('/recommendation', methods=('GET', 'POST'))
@login_required
def recommendation():
    if request.method == 'GET':
        # provide an option to upload the image and submit the for 
        return render_template('style_me/query_image.html')
    else:
        try:
            # save the file on server side 
            image_file = request.files['query-image']
            saved_image_path = QUERY_IMAGE_PATH + image_file.filename
            image_file.save(saved_image_path)
        except:
            return render_template('style_me/query_image.html')
        
        # get the fashion recommendation images list
        fashion_images_file_names = fr.get_recommendations(saved_image_path)
        fashion_type = retrieve_fashion_type(fashion_images_file_names[1]); 
        fashion_images = retrieve_images(fashion_images_file_names[1:])
        
        # provide the recommended options based upon the queried image
        return render_template('style_me/recommendation.html', fashion_images=fashion_images, 
                                fashion_type=fashion_type, query_image=image_file.filename) 

# gets the trending fashion images from the fashion gallery
def get_trendy_fashion_images():
    db = get_db()
    trendy_fashion_images = db.execute("""
                                            select * from fashion_gallery order by random() limit 30
                                       """).fetchall()
    return trendy_fashion_images

# tells if the given user is fashionista
def is_fashionista():
    db = get_db();
    fashionista = db.execute("""
                                select fashionista from user where id = (?)
                             """, (g.user['id'], )).fetchone()
    return dict(fashionista)['fashionista']


# fashion gallery for viewing the top trending images 
@bp.route('/fashion-gallery')
@login_required
def fashion_gallery():
    trendy_fashion_images = get_trendy_fashion_images()
    fashionista = is_fashionista()
    return render_template('style_me/fashion_gallery.html', fashion_images = trendy_fashion_images, fashionista=fashionista)


# fashion gallery for viewing the top trending images 
@bp.route('/fashion-gallery-static')
@login_required
def fashion_gallery_static():
    db = get_db()
    trendy_fashion_images = db.execute("""
                                            select * from fashion_gallery limit 30
                                       """).fetchall()
    fashionista = is_fashionista()
    return render_template('style_me/fashion_gallery.html', fashion_images = trendy_fashion_images, fashionista=fashionista)



# adds the given image to wishlist of the user
def add_to_user_wishlist(image_file_name):
    db = get_db()
    present_in_wish_list = db.execute("select * from wishlist where image_file_name = (?)",
                                      (image_file_name,)).fetchone()
    if(present_in_wish_list == None):
        user_id = g.user['id']
        db.execute("insert into wishlist(user_id, image_file_name) values(?,?)", (user_id, image_file_name))
        db.commit()
    else:
        return False

# deletes the given image file from the wishlist
def delete_to_user_wishlist(image_file_name):
    db = get_db()
    present_in_wish_list = db.execute("select * from wishlist where image_file_name = (?)",
                                      (image_file_name,)).fetchone()
    if(present_in_wish_list != None):
        user_id = g.user['id']
        db.execute("delete from wishlist where user_id = ? and image_file_name = ?", (user_id, image_file_name))
        db.commit()
    else:
        return False


@bp.route('/add-to-wishlist/<image_file_name>')
@login_required
def add_to_wishlist(image_file_name):
    if(add_to_user_wishlist(image_file_name)):
        # the image is added to the user wishlist in database
        flash('Image successfully added to wishlist')
    else:
        # the image already exits in the database
        flash('Image already present in wishlist')
    
    # finally after adding to the user wish list redirect to the wishlist pafe
    return redirect(url_for('style_me.user_wishlist'))


@bp.route('/delete-to-wishlist/<image_file_name>')
@login_required
def delete_to_wishlist(image_file_name):
    
    # the image already exits in the database
    delete_to_user_wishlist(image_file_name)
    flash('Image successfully deleted from wishlist')
    
    # finally after adding to the user wish list redirect to the wishlist pafe
    return redirect(url_for('style_me.user_wishlist'))


def get_wishist_image_file_names():
    db = get_db()
    user_id = g.user['id']
    data =  db.execute("select image_file_name from wishlist where user_id = (?)", (user_id ,)).fetchall()
    image_file_names = [dict(d)['image_file_name'] for d in data]
    return image_file_names


@bp.route('/my-wishlist')
@login_required
def user_wishlist():
    fashion_images = [dict(d) for d in retrieve_images(get_wishist_image_file_names())]
    return render_template('style_me/wishlist.html', fashion_images=fashion_images)


# gets the recommendation for the image file name 
@bp.route('/new-recommendation/<image_file_name>')
@login_required
def new_recommendation(image_file_name):
    query_image_path = './style_me/images/' + image_file_name
    fashion_images_file_names = fr.get_recommendations(query_image_path)
    
    # the fashion type is also stored in the database 
    fashion_type = retrieve_fashion_type(fashion_images_file_names[0].split("/")[-1]); 
    fashion_images = retrieve_images(fashion_images_file_names[1:])
    
    # provide the recommended options based upon the queried image
    return render_template('style_me/recommendation.html', fashion_images=fashion_images, fashion_type=fashion_type) 


# gets the fashion image
def get_fashion_image(image_file_name):
    db = get_db()
    fashion_image = db.execute("""
                                  select * from fashion_gallery where image_file_name = (?)
                               """, (image_file_name, )).fetchone()
    return dict(fashion_image)


# gets the comments done on the images 
def get_comments(image_file_name):
    db = get_db()
    comments = db.execute("""
                            select * from comments where image_file_name = (?)
                            order by created 
                          """, (image_file_name, )).fetchall()
    temp = [ dict(c) for c in comments ]
    return temp[::-1]


def get_username():
    db = get_db()
    user_id = g.user['id']
    username = db.execute("""
                            select username from user where id = (?)
                          """, (user_id, )).fetchone()
    return dict(username)['username']


# add a comment on fashion post 
@bp.route('/fashionista-comment/<image_file_name>', methods=('GET', 'POST'))
@login_required
def comment(image_file_name):
        
    if request.method == 'GET':
        # gets the fashion image
        fashion_image = get_fashion_image(image_file_name) 
    
        # get the comments on the fashion image 
        comments = get_comments(image_file_name)    
        
        # get if the person is fashionista
        fashionista = is_fashionista()

        # provide the recommended options based upon the queried image
        return render_template('style_me/comment.html',
                fashion_image=fashion_image, comments=comments, fashionista=fashionista) 
    else:
        db = get_db()
        comment_text = request.form['comment']
        username = get_username()
        db.execute("""
                    insert into comments(image_file_name, username, comment_text) values (?, ?, ?)
                   """, (image_file_name, username, comment_text))
        db.commit()

        # gets the fashion image
        fashion_image = get_fashion_image(image_file_name) 
    
        # get the comments on the fashion image 
        comments = get_comments(image_file_name)
        
        # get if the person is fashionista
        fashionista = is_fashionista()
        
        # provide the recommended options based upon the queried image
        return render_template('style_me/comment.html',
                fashion_image=fashion_image, comments=comments, fashionista=fashionista) 


# Style ME about page 
@bp.route('/about')
def about():
    return render_template('style_me/about.html')


# Style ME about data page
@bp.route('/about_data')
def about_data():
    return render_template('style_me/about_data.html')


# Style ME about model page 
@bp.route('/about_model')
def about_model():
    return render_template('style_me/about_model.html')


# Style ME about recommender page
@bp.route('/about_recommender')
def about_recommender():
    return render_template('style_me/about_recommender.html')


