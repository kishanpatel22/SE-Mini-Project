# Style-Me: Fashion Recommender
**WHAT TO WEAR??**  While going to a Club Party, should you wear a plain T-shirt wit a dark Denim or a Bomber Jacket with half cut Khaki Pants :confused: ? Not Sure whether a clothing style will make you look Cooler or a Nerd :nerd_face: ? Don't worry! We got you covered

Style Me has recommendation system that based on an image selected by the user can recommend similar options of clothings. 
Style Me is based on visual similarity recommendation engine and web scrapper to scrape images for various fashion websites.
This software gives a second pair of creative eyes to fashion designers and forecasters. 
Give power to small fashion designers and retailers to identify upcoming fashion trends quickly.

**Members- Div2 T2**

111803144 Manas Nighrunkar

111803172 Kishan Patel

111803181 Pranav Sadavarte

# Demo of Style-Me application

<a href="https://www.youtube.com/watch?v=Xgz555vPynE" 
frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen>
<img src="style_me/static/Store/images/rec.png" align="center">
</a>

## Motivation
Fashion Retailers try to draw inspiration from external sources such as e-commerce portals and online fashion magazines to design the next set of fashion products that they can launch in order to delight the customer. However, it is a manual effort intensive process, requiring a large team of fashion designers. In order to reduce dependency and make the overall process more efficient, we have created a scalable tech solution to extract winning designs of apparels to design upcoming batch of fashion products

## Features
* Sweet and Simple UI. Low effort experience is the key!
* Trending Images in the Fashion Gallery to checkout the latest fashion style!
* Product Reviews by Fashion Experts 
* User Wishlist
* Introducing New Products Regularly
* Various Product Categories
* Instant Recommendations based on the Query image uploaded by the User


![](style_me/static/Store/images/recommend.gif)

## RUN and Build the code 

* Make sure you have the libraries pre installed tensorflow, keras, pandas,
  selenium, BeautifulSoup, etc

* Install the code using the following commands 

```sh
    git clone git@gitlab.com:kishanpatel22/SE-Mini-Project.git
    cd SE-Mini-Project
    source .flaskenv 
```

* Make a few directories and files for the sqlite3 database

```sh
    mkdir instance
    touch instance/style_me.sqlite3
```

* Create directory for storing images file, query images and model

```sh
    cd style_me
    mkdir images
    mkdir query_images
    mkdir model
```

### Initializing the database -> Fashion gallery

* Note that this will scrape the images using chromium driver and places the 
  images in the database for the recommender initialization

```sh
    cd ../scrappers
    python3 init_fashion_db.py
```

### Precomputing the weights -> CNN resnet50

* Initialization of the recommendation algorithm 

```sh
    cd ../style_me
    python3 init_recommender.py
```

### Running flask 

* Before running flask ensure that you have source the file 

```sh
    flask init-db
    flask run 
```

### Server is Online!

* Open your favourite Web-Browser and type "http://127.0.0.1:5000" or simply "localhost:5000"

<img src="style_me/static/Store/images/Screenshot_from_2021-04-22_19-48-49.png" align="center">

#### That's it folks :smiley:! Checkout the Style-Me application, get cool fashion tips and recommendations & be ready to look AWESOME in front of your peers :sunglasses:
