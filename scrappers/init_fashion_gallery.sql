DROP TABLE IF EXISTS fashion_gallery;

CREATE TABLE fashion_gallery (
    image_id INTEGER PRIMARY KEY AUTOINCREMENT,
    image_file_url TEXT NOT NULL,
    image_file_name TEXT NOT NULL,
    ecommerce_link TEXT,
    price TEST,
    description TEXT,
    fashion_type TEXT
);
