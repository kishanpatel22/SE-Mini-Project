from selenium import webdriver
from bs4 import BeautifulSoup
import pandas as pd
import requests
import store_images

MEN_FASHION_LIST = ["casual-trousers", "formal-trousers", "jackets", "jeans", "shorts", 
                    "tshirts", "kurtas", "shirts", "pants", "suits", "nehru-jackets"]

class MyntraScrapper():

    def __init__(self, gender="men", search_depth=5):
        self.gender = gender
        self.driver_path = "./chromedriver"
        self.search_depth = search_depth
        self.myntra_url = "https://www.myntra.com"
        self.links_list, self.fashion_type = self.get_search_urls()
    
    """
        Gets all the search urls for myntra website
    """
    def get_search_urls(self):
        links_list = []
        fashion_type = []
        if self.gender == "men":
            fashion_list = MEN_FASHION_LIST
        else:
            fashion_list = WOMEN_FASHION_LIST
        for style in fashion_list:
            base_url = self.myntra_url + '/' + self.gender + '-' + style + '/'
            for i in range(1, self.search_depth + 1):
                new_url = base_url + '?p=' + str(i) 
                links_list.append(new_url)
                fashion_type.append(style)
        return (links_list, fashion_type)
    
    """
        Scrapes the fashion images and stores it in the given file path
    """
    def scrape_latest_fashion(self):
        Links=[] 
        Descriptions=[] 
        Price=[]
        BuyLinks=[]
        FashionType=[]
        driver = webdriver.Chrome(self.driver_path)
        for index, url in enumerate(self.links_list):
            driver.get(url)
            players = driver.find_elements_by_xpath('//img[@class="img-responsive"]')
            price = driver.find_elements_by_xpath('//span[@class="product-discountedPrice"]')              
            buylinks = driver.find_elements_by_xpath('//li[@class="product-base"]/a')              
            ftype = self.fashion_type[index]
            for i in range(len(players)):
                Links.append(str(players[i].get_attribute('src')))
                Descriptions.append(str(players[i].get_attribute('title')))
                Price.append(str(price[i].text))
                BuyLinks.append(buylinks[i].get_attribute('href'))
                FashionType.append(ftype)
        df = pd.DataFrame({'Product URL':Links, 'Descriptions':Descriptions, 'Price': Price,
                           'BuyLinks': BuyLinks, 'Type': FashionType})
        df.to_csv('my_fashion.csv', index=False, encoding='utf-8')

