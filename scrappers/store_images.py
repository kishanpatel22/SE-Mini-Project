import requests as re
import pandas as pd
import sqlite3

IMAGE_PATH = "../style_me/images/"
MY_FASHION_CSV = "my_fashion.csv"
DB_PATH = "../instance/style_me.sqlite"


def init_fashion_gallery():
    # open a connection to the database
    db_conn = sqlite3.connect(DB_PATH)
    db_script = open('init_fashion_gallery.sql', 'r').read()
    db_conn.executescript(db_script)
    print('Fashion gallery is initialized')
    return 


def count_images_db(db):
    count = db.execute("""
                            select count(*) from fashion_gallery
                       """).fetchone()[0]
    return count

def store_image_db(db, url_link, file_name, description, ecommerce_link, price, fashion_type):
    db.execute("""
                    insert into fashion_gallery 
                    (image_file_url, image_file_name, description, ecommerce_link, price, fashion_type) 
                    values (?, ?, ?, ?, ?, ?)
               """, (url_link, file_name, description, ecommerce_link, price, fashion_type))
    return 

"""
    Downloads the image dataset for the given dataframe of the format
    "URL for image", "Image description"
    Note the function also updates the database for the give image
"""

def store_images(df):

    # open a connection to the database
    db_conn = sqlite3.connect(DB_PATH)
    db_cur = db_conn.cursor()
    count_images = count_images_db(db_cur)
    
    # get all the url links of the images 
    url_links = list(df['Product URL'])

    for file_no, url_link in enumerate(url_links):

        # get the extension of the file 
        ext = url_link.split('.')[-1]
        response = re.get(url_link)

        # get the full file path where image will be downloaded 
        file_name = "file_" + str(file_no + count_images + 1) + "." + ext
            
        # description about the image
        description = df.loc[file_no]['Descriptions']
        
        ecommerce_link = df.loc[file_no]['BuyLinks']
        price = df.loc[file_no]['Price']
        fashion_type = df.loc[file_no]['Type']

        # store the details about the image in the database 
        store_image_db(db_cur, url_link, file_name, description, ecommerce_link, price, fashion_type)
        db_conn.commit()
        
        file_path = IMAGE_PATH + file_name
        # write the image on to the file system 
        fd = open(file_path , "wb")
        fd.write(response.content)
        fd.close()

        print("downloading image ===> ", file_path)

    db_conn.close()

"""
    Starts downloading all the fashion images and stores the images in the database
"""
def start_downloading():
    df = pd.read_csv(MY_FASHION_CSV)
    store_images(df)

